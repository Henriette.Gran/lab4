package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows; 
    private int cols;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;

        this.grid = new CellState[rows][cols];

        for (int i=0; i<rows; i++){
            
            for (int j=0; j<cols; j++){

                grid[i][j] = initialState;
            }

        }

	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        
        if (row<0 || row>= numRows() || column<0 || column>= numColumns()){
            throw new IndexOutOfBoundsException("Index" + row + ", " + column + " is out of bounds.");
        }
        
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        
        
        if (row<0 || row>= numRows() || column<0 || column>= numColumns()){
            throw new IndexOutOfBoundsException("Index" + row + ", " + column + " is out of bounds.");
        }



        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        
        CellGrid gridcopy = new CellGrid(rows, cols, CellState.ALIVE);

        for (int i=0; i<rows; i++){
            for (int j=0; j<cols; j++){

                gridcopy.grid[i][j]= get(i,j);
            }
        }
    
        return gridcopy;
    }
    
}
